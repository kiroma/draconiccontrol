local component = require("component")
local reactor = component.draconic_reactor
local gate = component.flux_gate
local maxtemp = 7999.98

local run = true
while run do
  local exponent = 2
  local info = reactor.getReactorInfo
  if info.temperature > maxtemp then
    exponent = 5
  end
  local targetDraw = info.generationRate*((maxtemp/info.temperature)^exponent)
  gate.setSignalLowFlow(targetDraw)
end
