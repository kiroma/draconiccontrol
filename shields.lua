local component = require("component")
local event = require("event")
 
local reactor = component.draconic_reactor
local gate = component.flux_gate
 
local run = true
while run do
    local e = event.pull(0, "touch")
    if(e) then
        run = false
    end
    local info = reactor.getReactorInfo()
    gate.setSignalLowFlow(info.fieldDrainRate*(4/3))
end